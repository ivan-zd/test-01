# Local library

Django example for CBV implementation

### Quickstart

Create Python3 virtual environment:

    mkvirtualenv --python=/usr/bin/python3 local-lib

Get the source from GitHub:

    git clone git@github.com:HBalija/locallibrary.git

Navigate to the project directory:

    cd locallibrary

Instal requirements:

    pip install -r requirements.txt

Run development server:

    python manage.py runserver

Point your browser to:

    127.0.0.1:8000
