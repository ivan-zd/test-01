from django.contrib import admin

from .models import Author, Genre, Book, BookInstance


class AuthorAdmin(admin.ModelAdmin):
    """Display model fields."""
    list_display = ('last_name', 'first_name', 'date_of_birth', 'date_of_death')

    # fields attribute lists those fields that are to be displayed on the form, in order
    # tuple them to put them in the same line
    fields = ['first_name', 'last_name', ('date_of_birth', 'date_of_death')]


class BooksInstanceInline(admin.TabularInline):
    """Horizontal inline. Stacked --> vertical."""
    model = BookInstance


@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    """Genre is many2many field --> see Book model."""
    list_display = ('title', 'author', 'display_genre')

    # adding tabularinline
    inlines = [BooksInstanceInline]


@admin.register(BookInstance)
class BookInstanceAdmin(admin.ModelAdmin):
    """Add filtering by genre to BookInstance list."""
    list_filter = ('status', 'due_back')
    list_display = ('book', 'status', 'borrower', 'due_back', 'id')

    # sectioning fields with fieldsets
    fieldsets = (
        (None, {
            'fields': ('book', 'imprint', 'id')
        }),
        ('Availability', {
            'fields': ('status', 'due_back', 'borrower')
        }),
    )


admin.site.register(Author, AuthorAdmin)
admin.site.register(Genre)
